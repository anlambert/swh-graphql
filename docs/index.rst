.. _swh-graphql:

Software Heritage - GraphQL APIs
================================

.. only:: standalone_package_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
