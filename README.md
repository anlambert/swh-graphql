# swh-graphql

## dev mode inside docker

* make run-dev-docker

## dev mode without docker

* make run-dev

## dev mode without auto reload

* make run-dev-stable

## staging mode inside docker

* make run-staging

## visit

* http://localhost:8000/
